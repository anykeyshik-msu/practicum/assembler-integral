global f1, f2, f3
global df1, df2, df3
global ddf1, ddf2, ddf3

section .data
    f1_const1 dq 0.35
    f1_const2 dq -0.95
    f1_const3 dq 2.7

    df1_const1 dq 0.7
    df1_const2 dq -0.95

    ddf1_const1 dq 0.7

    f2_const1 dq 3.0
    f2_const2 dq 1.0

    df2_const1 dq 3.0

    ddf2_const1 dq 0.0

    f3_const1 dq 1.0
    f3_const2 dq 2.0

    df3_const1 dq -1.0
    df3_const2 dq 2.0

    ddf3_const1 dq 2.0
    ddf3_const2 dq 2.0

section .text
; f = 0.35 * x ^ 2 - 0.95 * x + 2.7 
f1:
    push ebp
    mov ebp, esp

    fld qword [ebp + 8]
    fld qword [ebp + 8]
    fld qword [f1_const1]
    fmulp
    fmulp

    fld qword [ebp + 8]
    fld qword [f1_const2]
    fmulp

    fld qword [f1_const3]
    faddp

    faddp

    leave
    ret

; f = 0.7 * x - 0.95
df1:
    push ebp
    mov ebp, esp
    
    fld qword [ebp + 8]
    fld qword [df1_const1]
    fmulp

    fld qword [df1_const2]
    fsubp

    leave
    ret

; f = 0.7
ddf1:
    push ebp
    mov ebp, esp

    fld qword [ddf1_const1]

    leave
    ret

; f = 3 * x + 1
f2:
    push ebp
    mov ebp, esp

    fld qword [ebp + 8]
    fld qword [f2_const1]
    fmulp

    fld qword [f2_const2]
    faddp

    leave
    ret

; f = 3
df2:
    push ebp
    mov ebp, esp

    fld qword [df2_const1]

    leave
    ret

; f = 0
ddf2:
    push ebp
    mov ebp, esp

    fld qword [ddf2_const1]

    leave
    ret

; f = 1 / (x + 2)
f3:
    push ebp
    mov ebp, esp

    fld qword [f3_const1]
    fld qword [ebp + 8]
    fld qword [f3_const2]
    faddp

    fdivp

    leave
    ret

; f = -1 / (x + 2) ^ 2
df3:
    push ebp
    mov ebp, esp

    fld qword [df3_const1]

    fld qword [ebp + 8]
    fld qword [ddf3_const2]
    faddp
    fld qword [ebp + 8]
    fld qword [ddf3_const2]
    faddp
    fmulp

    fdivp

    leave
    ret

; f = 2 / (x + 2) ^ 3
ddf3:
    push ebp
    mov ebp, esp

    fld qword [ddf3_const1]
    
    fld qword [ebp + 8]
    fld qword [ddf3_const2]
    faddp
    fld qword [ebp + 8]
    fld qword [ddf3_const2]
    faddp
    fmulp
    fld qword [ebp + 8]
    fld qword [ddf3_const2]
    faddp
    fmulp

    fdivp

    leave
    ret
