#include <stdio.h>
#include <float.h>
#include <math.h>
#include "user_handler.h"

extern double f1(double x);
extern double f2(double x);
extern double f3(double x);

extern double df1(double x);
extern double df2(double x);
extern double df3(double x);

extern double ddf1(double x);
extern double ddf2(double x);
extern double ddf3(double x);

// Border points of intersection
const double a = -1.9;
const double b = 1.9;

// Calc deltas
const double equation_eps = 0.000001;
const double integral_eps = 0.000001;

// Flags for show points and count iterations
int show_points = 0;
int count_iterations = 0;

// Func and pointer for integral
double
solve_integ(double x);
double (*h1)(double);
double (*h2)(double);

// Approx solution of equation
double
combined_method(double a, double b, double eps, 
        double (*f1)(double), double (*f2)(double),
        double (*df1)(double), double (*df2)(double),
        double (*ddf1)(double), double (*ddf2)(double));

// Quadrature formula 
double
integrate(double a, double b, double eps, double (*g1)(double));

// Solve of f1(x) = f2(x)
double
equation_solve(double (*f1)(double), double (*f2)(double), double x);

// Test function for integral
void
test(int test_type);

void
swap(double *first, double *second);

int 
main(int argc, char **argv) 
{ 
    double f1_f2;
    double f2_f3;
    double f1_f3;
    double I1, I2;

    int key = key_checker(argc, argv);
    if (key == KEY_EXIT) {
        return 0;
    }
    else if (key == KEY_POINTS) {
        show_points = 1;
    }
    else if (key == KEY_ITERATIONS) {
        count_iterations = 1;
    }
    else if ( (key & 204) == 204 ) {
        test(key);

        return 0;
    }

    printf("Calculating root for f1 and f2...\n");
    f1_f2 = combined_method(a, b, equation_eps, 
            f1, f2,
            df1, df2,
            ddf1, ddf2);
    printf("Done!\n");
    printf("Calculating root for f2 and f3...\n");
    f2_f3 = combined_method(a, b, equation_eps, 
            f2, f3,
            df2, df3,
            ddf2, ddf3);
    printf("Done!\n");
    printf("Calculating root for f1 and f3...\n");
    f1_f3 = combined_method(a, b, equation_eps, 
            f1, f3,
            df1, df3,
            ddf1, ddf3);
    printf("Done!\n");

    h1 = f1;
    h2 = f2;
    I1 = integrate(f1_f2, f2_f3, integral_eps, solve_integ);
    h1 = f1;
    h2 = f3;
    I2 = integrate(f2_f3, f1_f3, integral_eps, solve_integ);

    printf("%lf\n", fabs(I1 + I2));

    return 0;
}

double
combined_method(double a, double b, double eps, 
        double (*f1)(double), double (*f2)(double),
        double (*df1)(double), double (*df2)(double),
        double (*ddf1)(double), double (*ddf2)(double))
{
    double f_a;
    double f_b;
    int counter = 0;

    while (fabs(a - b) > 2 * eps) {
        f_a = equation_solve(f1, f2, a);
        f_b = equation_solve(f1, f2, b);

        if (f_a * equation_solve(ddf1, ddf2, a) < 0) {
            double res = equation_solve(f1, f2, a) - equation_solve(f1, f2, b);

            a -= f_a * (a - b) / res;
        }
        else if (f_a * equation_solve(ddf1, ddf2, a) > 0) {
            a -= f_a / equation_solve(df1, df2, a);
        }

        if (f_b * equation_solve(ddf1, ddf2, b) < 0) {
            double res = equation_solve(f1, f2, b) - equation_solve(f1, f2, a);

            b -= f_b * (b - a) / res;
        }
        else if (f_b * equation_solve(ddf1, ddf2, b) > 0) {
            b -= f_b / equation_solve(df1, df2, b);
        }

        counter++;
    }

    if (show_points) {
        printf("Solve for this equation is %lf.\n", (a + b) / 2);
    }

    if (count_iterations) {
        printf("%d iterations was requried for solve this equation.\n", counter);
    }

    return (a + b) / 2;
}

double
integrate(double a, double b, double eps, double (*f)(double))
{
    // Repair integral sign
    if (b < a) {
        swap(&a, &b);
    }

    double ans;
    double prev_ans; // For calc answers diff
    int iterations;

    prev_ans = DBL_MAX;
    ans = 0.0;
    iterations = 4;

    while (fabs(ans - prev_ans) >= eps) {
        prev_ans = ans;
        ans = 0;
        double width = (b - a) / iterations;

        for(int step = 0; step < iterations; step++) {
            const double x1 = a + step * width;
            const double x2 = a + (step + 1) * width;

            ans += 0.5 * (x2 - x1) * (f(x1) + f(x2));
        }

        iterations *= 2;
    }

    return ans;
}

double
solve_integ(double x)
{
    return h1(x) - h2(x);
}

double
equation_solve(double (*f1)(double), double (*f2)(double), double x) 
{
    return f1(x) - f2(x);
}

void
test(int test_type)
{
    double ans = 0.0;

    if (test_type == KEY_TEST_INTEGRAL) {
        double a, b;
        double eps;
        int number;

        printf("For calculating the integral of f# on section [a; b] with epsilon precision please enter\n");
        printf("Number of function (1, 2, 3): ");
        scanf("%d", &number);
        printf("Seciton [a; b]: ");
        scanf("%lf %lf", &a, &b);
        printf("Epsilon precision: ");
        scanf("%lf", &eps);

        switch (number) {
            case 1:
                ans = integrate(a, b, eps, f1);
                break;
            case 2:
                ans = integrate(a, b, eps, f2);
                break;
            case 3:
                ans = integrate(a, b, eps, f3);
                break;
            default:
                print_error(UNKNOWN_FUNCTION);
                return;
        }
        
        printf("Integral of given function ~ ");
    }
    else if (test_type == KEY_TEST_ROOTS) {
        double a, b;
        double eps;
        int number1, number2;
        double (*g1)(double), (*dg1)(double), (*ddg1)(double);
        double (*g2)(double), (*dg2)(double), (*ddg2)(double);

        printf("To find the root of the equation `f#1 - f#2` on seciton [a; b] with epsilon precision please enter\n");
        printf("Number of two functions (1, 2, 3): ");
        scanf("%d %d", &number1, &number2);
        printf("Section [a; b]: ");
        scanf("%lf %lf", &a, &b);
        printf("Epsilon precision: ");
        scanf("%lf", &eps);

        switch (number1) {
            case 1:
                g1 = f1;
                dg1 = df1;
                ddg1 = ddf1;
                break;
            case 2:
                g1 = f2;
                dg1 = df2;
                ddg1 = ddf2;
                break;
            case 3:
                g1 = f3;
                dg1 = df3;
                ddg1 = ddf3;
                break;
            default:
                print_error(UNKNOWN_FUNCTION);
                return;
        }

        switch (number2) {
            case 1:
                g2 = f1;
                dg2 = df1;
                ddg2 = ddf1;
                break;
            case 2:
                g2 = f2;
                dg2 = df2;
                ddg2 = ddf2;
                break;
            case 3:
                g2 = f3;
                dg2 = df3;
                ddg2 = ddf3;
                break;
            default:
                print_error(UNKNOWN_FUNCTION);
                return;
        }

        if (g1 == g2) {
            print_error(SAME_FUNCTIONS);
        }

        ans = combined_method(a, b, eps, 
                g1, g2,
                dg1, dg2,
                ddg1, ddg2);
        printf("Root of the given equation ~ ");
    }

    printf("%lf", ans);
}

void
swap(double *a, double *b)
{
    double tmp = *a;
    *a = *b;
    *b = tmp;
}
