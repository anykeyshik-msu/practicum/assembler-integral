# Tools
CC := gcc
ASM := nasm

# Flags
CFLAGS += -O2 -Wall -Werror -Wformat-security -Wignored-qualifiers -Winit-self -Wswitch-default -Wfloat-equal -Wpointer-arith -Wtype-limits -Wempty-body -Wno-logical-op -Wstrict-prototypes -Wold-style-declaration -Wold-style-definition -Wmissing-parameter-type -Wmissing-field-initializers -Wnested-externs -Wno-pointer-sign -Wno-unused-result -std=gnu99 -lm -g -m32
AFLAGS += -felf32

# Target
TARGET_DIR = bin
EXECUTABLE := integral
TARGET := $(TARGET_DIR)/$(EXECUTABLE)

# Dirs
SOURCE_DIR := src
BUILD_DIR := build
INCLUDE_DIR := headers

# Files
ASM_SRC_FILES := $(wildcard src/*.s)
ASM_OBJ_FILES := $(patsubst src/%.s, build/%.o, $(ASM_SRC_FILES))
C_SRC_FILES := $(wildcard src/*.c)
C_OBJ_FILES := $(patsubst src/%.c, build/%.o, $(C_SRC_FILES))

.PHONY: all clean run

all: $(TARGET)

clean:
	@echo "Cleaning all..."
	@rm -rf build bin

run: $(TARGET)
	@$^

$(BUILD_DIR)/%.o: $(SOURCE_DIR)/%.c
	@mkdir -p $(BUILD_DIR)
	$(CC) $(CFLAGS) -c -I $(INCLUDE_DIR) $< -o $@

$(BUILD_DIR)/%.o: $(SOURCE_DIR)/%.s
	@mkdir -p $(BUILD_DIR)
	$(ASM) $(AFLAGS) -o $@ $^

$(TARGET): $(C_OBJ_FILES) $(ASM_OBJ_FILES)
	@mkdir -p $(TARGET_DIR)
	$(CC) $(CFLAGS) -o $@ $(C_OBJ_FILES) $(ASM_OBJ_FILES)
