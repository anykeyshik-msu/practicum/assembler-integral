#pragma once
#ifndef USER_HANDLER_H
#define USER_HANDLER_H

#include <stdio.h>
#include <string.h>

// Errors define
#define NOT_ENOUGH_ARGS     0
#define UNKNOWN_PARAM       1
#define UNKNOWN_KEY         2
#define UNKNOWN_FUNCTION    3
#define SAME_FUNCTIONS      4

// Key checker returns defines
#define KEY_NOTHING         200
#define KEY_EXIT            201
#define KEY_POINTS          202
#define KEY_ITERATIONS      203
#define KEY_TEST_ROOTS      204
#define KEY_TEST_INTEGRAL   205

static inline void
print_usage(void);

static inline void 
print_error(int error_code);

// Processing cli keys what have been passed to program
static inline int 
key_checker(int argc, char **argv)
{
    for (int i = 1; i < argc; i++) {
        argv[i] = argv[i];

        if ( !strcmp(argv[i], "-h") || !strcmp(argv[i], "--help") ) {
            print_usage();

            return KEY_EXIT;
        } 
        else if ( !strcmp(argv[i], "-t") || !strcmp(argv[i], "--test") ) {
            if (++i == argc) {
                print_error(NOT_ENOUGH_ARGS);

                return KEY_EXIT;
            }
            else {
                if ( !strcmp(argv[i], "root") ) {
                    return KEY_TEST_ROOTS;
                }
                else if ( !strcmp(argv[i], "integral") ) {
                    return KEY_TEST_INTEGRAL;
                }
                else {
                    print_error(UNKNOWN_PARAM);
                
                    return KEY_EXIT;
                }
            }
        }
        else if ( !strcmp(argv[i], "-s") || !strcmp(argv[i], "--show") ) {
            if (++i == argc) {
                print_error(NOT_ENOUGH_ARGS);

                return KEY_EXIT;
            }
            else {
                if ( !strcmp(argv[i], "iterations") ) {
                    return KEY_ITERATIONS;
                }
                else if ( !strcmp(argv[i], "points") ) {
                    return KEY_POINTS;
                }
                else {
                    print_error(UNKNOWN_PARAM);

                    return KEY_EXIT;
                }
            }
        }
        else {
            print_error(UNKNOWN_KEY);

            return KEY_EXIT;
        }
    }

    return KEY_NOTHING;
}

static inline void
print_usage(void)
{
    printf("Calculating the area of the figure what created by intersections of the functions\n");
    printf("f1(x) = 0.35 * x ^ 2 - 0.95 * x + 2.7\n");
    printf("f2(x) = 3 * x + 1\n");
    printf("f3(x) = 1 / (x + 2)\n");
    printf("\t-h, --help\t\tshows all valid keys\n");
    printf("\t-t, --test TEST\t\tfor test. Possible TEST: root, integral\n");
    printf("\t-s, --show SHOW\t\tfor show service info. SHOW: iterations, points\n");
}

static inline void
print_error(int error_code)
{
    printf("ERROR: ");

    switch (error_code) 
    {
        case NOT_ENOUGH_ARGS:
            printf("Not enough arguments for this key!\n");
            break;
        case UNKNOWN_PARAM:
            printf("Unknown parameter for this key!\n");
            break;
        case UNKNOWN_KEY:
            printf("Unknown key!\n");
            break;
        case UNKNOWN_FUNCTION:
            printf("Unknown function number!\n");
            break;
        case SAME_FUNCTIONS:
            printf("Same functions in equation!\n");
            break;
        default:
            printf("Unknown error!\n");
    }
}

#endif
